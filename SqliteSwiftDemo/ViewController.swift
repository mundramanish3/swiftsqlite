//
//  ViewController.swift
//  test
//
//  Created by indianic on 27/12/16.
//  Copyright © 2016 manish. All rights reserved.
//

import UIKit

let INSERT_QUERY = "INSERT INTO Employee (name, cell) VALUES ('indianic', 123)"
let DELETE_QUERY = "DELETE FROM Employee"
let UPDATE_QUERY = "UPDATE Employee  SET name='Test' WHERE name='indianic'"
let GET_RECORD_COUNT = "SELECT COUNT(*) FROM Employee"
let INSERT_BULK = "INSERT INTO Employee (name, cell) VALUES (?,?)"
let CREATE_TABLE = "CREATE TABLE if not exists test (id integer primary key autoincrement, name text)"


class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        copyDB()
        
        insert()
        
        update()
        
        delete()
        
        getRecordCount()
        
        createTable()
        
        insertInBulk()
        
        fetchAllData()

    }

    
    func copyDB() {
        Database.sharedInstance.createEditableCopyOfDatabaseIfNeeded {
            // Failure
            
            let alert = UIAlertController(title: "Error!!", message: "Failed to create writable database", preferredStyle: .alert)
            let action =  UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction) in
                print("Alert Button Clicked")
                
            })
            
            alert.addAction(action)
            
            present(alert, animated: true, completion: nil)
            
        }
    }
    
    func insert() {
        // Insert
        Database().insert(query:INSERT_QUERY , success: {
            
            print("Successful Insertion")
        }) { (error: String) in
            print("Error: \(error)")
        }
        
    }
    
    func update() {
        // Update
        Database().update(query:UPDATE_QUERY , success: {
            // Success
            
            print("Updation Successfully")
        }) { (error: String) in
            // Failure
            
            print("Updation Failure")
        }
    }
    
    func delete() {
        // Delete
        Database().delete(query:DELETE_QUERY , success: {
            // Success
            
            print("Deletion Successfully")
        }) { (error: String) in
            // Failure
            
            print("Deletion Failure")
        }
    }
    
    func getRecordCount() {
        
        Database().getRecordCount(query:GET_RECORD_COUNT , success: { (count: Int) in
            print("Get Record Count Success : \(count)")
        }) { (error: String) in
            // Failure
            
            print("Get Record Count Failure = \(error)")
        }
    }
    
    func createTable() {
        
        Database().createTable(query:CREATE_TABLE , success: {
            
        }) { (error: String) in
            print(error)
        }
    }
    
    func insertInBulk() {
        
        var aArr = [[String: AnyObject]]()
        
        aArr = [["name" : "Manish" as AnyObject, "cell":123 as AnyObject],
                ["name" : "Manish" as AnyObject, "cell":123 as AnyObject]]
        
        Database().insertDatinBulk(query:INSERT_BULK , array: aArr) { (error: String) in
            print("Error: \(error)")
        }
    }
    
    func fetchAllData() {
        let aArrSelectedResult = Database().getAllData(query: "SELECT * FROM Employee") { (error: String) in
            print(error)
        }
        
        
        print(aArrSelectedResult)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

