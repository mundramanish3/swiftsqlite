Constant Queries 

let INSERT_QUERY = "insert into Employee (name, cell) values ('indianic', 123)"
let DELETE_QUERY = "delete from Employee"
let UPDATE_QUERY = ""
let GET_RECORD_COUNT = "SELECT COUNT(*) FROM Employee"
let INSERT_BULK = "INSERT INTO Employee (name, cell) VALUES (?,?)"
let CREATE_TABLE = "create table if not exists test (id integer primary key autoincrement, name text)"
       
Calling 

	 copyDB()
        insert()
        update()
        delete()
        getRecordCount()
        createTable()
        insertInBulk()
        fetchAllData()



    func copyDB() {
        Database.sharedInstance.createEditableCopyOfDatabaseIfNeeded {
            // Failure
            
            let alert = UIAlertController(title: "Error!!", message: "Failed to create writable database", preferredStyle: .alert)
            let action =  UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction) in
                print("Alert Button Clicked")
                
            })
            
            alert.addAction(action)
            
            present(alert, animated: true, completion: nil)
            
        }
    }
    
    func insert() {
        // Insert
        Database().insert(query:INSERT_QUERY , success: {
            
            print("Successful Insertion")
        }) { (error: String) in
            print("Error: \(error)")
        }
        
    }
    
    func update() {
        // Update
        Database().update(query:UPDATE_QUERY , success: {
            // Success
            
            print("Updation Successfully")
        }) { (error: String) in
            // Failure
            
            print("Updation Failure")
        }
    }
    
    func delete() {
        // Delete
        Database().delete(query:DELETE_QUERY , success: {
            // Success
            
            print("Deletion Successfully")
        }) { (error: String) in
            // Failure
            
            print("Deletion Failure")
        }
    }
    
    func getRecordCount() {
        
        Database().getRecordCount(query:GET_RECORD_COUNT , success: { (count: Int) in
            print("Get Record Count Success : \(count)")
        }) { (error: String) in
            // Failure
            
            print("Get Record Count Failure = \(error)")
        }
    }
    
    func createTable() {
        
        Database().createTable(query:CREATE_TABLE , success: {
            
        }) { (error: String) in
            print(error)
        }
    }
    
    func insertInBulk() {
        
        var aArr = [[String: AnyObject]]()
        
        aArr = [["name" : "Manish" as AnyObject, "cell":123 as AnyObject],
                ["name" : "Manish" as AnyObject, "cell":123 as AnyObject]]
        
        Database().insertDatinBulk(query:INSERT_BULK , array: aArr) { (error: String) in
            print("Error: \(error)")
        }
    }
    
    func fetchAllData() {
        let aArrSelectedResult = Database().getAllData(query: "SELECT * FROM Employee") { (error: String) in
            print(error)
        }
        
        
        print(aArrSelectedResult)
    }